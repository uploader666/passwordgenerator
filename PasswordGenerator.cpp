// PasswordGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

static const char alphanum[] =
"0123456789"
"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

using namespace std;

int stringLength = sizeof(alphanum) - 1;

char genRandom()
{

	return alphanum[rand() % stringLength];
}


int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(0));
	string Str;
	int i, n;

	do
	{
		cout << "Enter password length: ";
		cin >> n;
		Str = "";
		for (unsigned int i = 0; i < n; ++i)
		{
			Str += genRandom();

		}
		cout << Str << endl;
	} while (n != 0);
	return 0;
}

